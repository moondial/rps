import random
import json
import os

rolls = {}


def main():
    load_rolls()
    header()

    user = input('name? ')
    play_game(user, "Computer")


def header():
    print("--------------------------------")
    print("   Rock Paper Scissors v2")
    print("   Extensible Rules Edition")
    print("--------------------------------")


def get_players():
    pass # 6:13 - demo: saving wins 9/12


def play_game(player_1, player_2):
    wins = {player_1: 0, player_2: 0}

    roll_names = list(rolls.keys())

    while not find_winner(wins, wins.keys()):
        roll_1 = get_roll(player_1, roll_names)
        roll_2 = random.choice(roll_names)

        if not roll_1:
            print("Can't play that, try again")
            continue

        print(f"{player_1} rolls {roll_1}")
        print(f"{player_2} rolls {roll_2}")

        winner = check_for_winning_roll(player_1, player_2, roll_1, roll_2)

        if winner is None:
            print("This round is a tie")
        else:
            print(f"{winner} takes the round!")
            wins[winner] += 1
        print(f"Score is {player_1}: {wins[player_1]} and {player_2}: {wins[player_2]}. ")
        print()

    # if wins_p1 >= rounds:
    #     overall_winner = player_1
    # else:
    #     overall_winner = player_2
    overall_winner = find_winner(wins, wins.keys())

    print(f"{overall_winner} wins the game!")


def find_winner(wins, names):
    best_of = 3
    for name in names:
        if wins.get(name, 0) >= best_of:
            return name

    return None


def check_for_winning_roll(player_1, player_2, roll_1, roll_2):
    # Test for a winner
    winner = None
    if roll_1 == roll_2:
        print("The game was a tie")

    outcome = rolls.get(roll_1, {})
    if roll_2 in outcome.get('defeats'):
        return player_1
    elif roll_2 in outcome.get('defeated_by'):
        return player_2

    return winner


def get_roll(player_1, roll_names):
    print("Available rolls:")
    for index, r in enumerate(roll_names, start=1):
        print(f"{index}. {r}")
        index += 1

    text = input(f'{player_1} what would you like to roll? [rock, paper, scissors, air, fire, water]: ')
    selected_index = int(text) - 1

    if selected_index < 0 or selected_index > len(rolls):
        print(f'Not a valid roll {player_1}! {text} is not valid.')
        return None
    return roll_names[selected_index]


def load_rolls():
    global rolls

    directory = os.path.dirname(__file__)

    filename = os.path.join(directory, 'rolls.json')

    #print(filename)

    with open(filename, 'r', encoding='utf-8') as fin:
        rolls = json.load(fin)
    print(f"loaded rolls {list(rolls.keys())}")


if __name__ == '__main__':
    main()

# Rock
    #     Rock -> tie
    #     Paper -> lose
    #     Scissors -> win
    #Paper
    #     Rock -> win
    #     Paper -> tie
    #     Scissors -> lose
    #Scissors
    #     Rock -> lose
    #     Paper -> win
    #     Scissors -> tie
